/*Função que pega o click dos números e caracteres de operação*/
function pegarNumero(num){
   let valor1 = document.getElementById('resultado').values;
   document.getElementById('resultado').value = valor1 + num;
}

/*Função que limpa atela completamente*/
function clean(){
   document.getElementById('resultado').value = "";
}

/*Função que apaga o último caractere*/
function back(){
   let resultado = document.getElementById('resultado').value;
   document.getElementById('resultado').value = resultado.substring(0, resultado.length -1)
}

/*Função que faz os cálculos*/
function fazerCalculo(){
   let resultado = document.getElementById('resultado').value;
   
   if(resultado){ 
      document.getElementById('resultado').values = eval(resultado);
   }else{
      document.getElementById('resultado').values = "";
   }
}
