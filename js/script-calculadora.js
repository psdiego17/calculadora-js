class Calculadora{
    /*Contrutuor*/
    constructor(){
        this.tela = document.getElementById('resultado')
        this.pegarClickBotao()
    }
        

    /*Função com Evento dos que pega os botões da calculadora*/
    pegarClickBotao(){
        /*for(let i=0; i<=9; i++){
        document.getElementById('' + i).addEventListener('click', this.inserirNumeroVisor)*/

        /*Botões númericos*/ 
        document.getElementById('0').addEventListener("click",() => this.inserirNumeroVisor('0'))
        document.getElementById('1').addEventListener("click",() => this.inserirNumeroVisor('1'))
        document.getElementById('2').addEventListener("click",() => this.inserirNumeroVisor('2'))
        document.getElementById('3').addEventListener("click",() => this.inserirNumeroVisor('3'))
        document.getElementById('4').addEventListener("click",() => this.inserirNumeroVisor('4'))
        document.getElementById('5').addEventListener("click",() => this.inserirNumeroVisor('5'))
        document.getElementById('6').addEventListener("click",() => this.inserirNumeroVisor('6'))
        document.getElementById('7').addEventListener("click",() => this.inserirNumeroVisor('7'))
        document.getElementById('8').addEventListener("click",() => this.inserirNumeroVisor('8'))
        document.getElementById('9').addEventListener("click",() => this.inserirNumeroVisor('9'))
        
        /*Botão de Limpar a tela toda*/
        document.getElementById("ce").addEventListener("click", () => this.limparTela())
        
        /*Botões das operações*/
        document.getElementById('+').addEventListener("click",() => this.inserirNumeroVisor('+'))
        document.getElementById('-').addEventListener("click",() => this.inserirNumeroVisor('-'))
        document.getElementById('*').addEventListener("click",() => this.inserirNumeroVisor('*'))
        document.getElementById('/').addEventListener("click",() => this.inserirNumeroVisor('/'))

        /*Botão de igualdade*/
        document.getElementById('i').addEventListener("click",() => this.fazerCalculo())

        /*Botão de apagar último caractere*/
        document.getElementById("c").addEventListener("click",() => this.apagarUltimoCaractere())
        
        /*Botão do ponto*/
        document.getElementById(".").addEventListener("click",() => this.inserirNumeroVisor('.'))
    }

    /*Função que insere o número no visor*/
    inserirNumeroVisor(num){
       //this.tela.value += num
       let valor1 = document.getElementById('resultado').value
       this.tela.value = valor1 + num
    }

    /*Função que faz os cálculos*/
    fazerCalculo(){
        let resultado = document.getElementById('resultado').value;
        if(this.tela){
            document.getElementById('resultado').value = eval(this.tela.value)
        }
        else{
            resultado.value = "0"
        }
    }

    /*Função que limpa a tela completamenta*/
    limparTela(){
        this.tela.value = ""
    }

    /*Função que apaga o último caractere*/
    apagarUltimoCaractere(){
        let resultado = document.getElementById('resultado').value;
        document.getElementById('resultado').value = resultado.substring(0, resultado.length -1)
    }
}  
let calculadora = new Calculadora()
